#pragma once
#include <stdbool.h>

// Direction codes used in various contexts
typedef enum {
    NORTH, EAST, SOUTH, WEST, UNKNOWN
} DIRECTION;

// Possible contents of territory cell
typedef enum {
    EMPTY,
    WALL,
    PAINT,
    MARKER
} CELL;

// Territory constant dimensions
enum {
    TERRITORY_WIDTH = 46,
    TERRITORY_HEIGHT = 23
};

// Global territory array
extern CELL territory[TERRITORY_HEIGHT][TERRITORY_WIDTH];

void initTerritory();
bool translateCell(int* y, int* x, DIRECTION dir);
