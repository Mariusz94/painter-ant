#include "territory.h"

// The definition of territory
CELL territory[TERRITORY_HEIGHT][TERRITORY_WIDTH];

/**
 * @brief  Initializes territory with empty cells and walls around perimeter
 */
void initTerritory() {
    int y, x;
    for (y = 0; y < TERRITORY_HEIGHT; y++) {
        for (x = 0; x < TERRITORY_WIDTH; x++) {
            if (y == 0 || y == TERRITORY_HEIGHT-1 ||
                x == 0 || x == TERRITORY_WIDTH-1) {
                territory[y][x] = WALL;
            } else {
                territory[y][x] = EMPTY;
            }
        }
    }
}

/**
 * @brief   Moves point coordinates in given direction respecting
 *          territory size bounds
 * @param [in,out] y    Pointer to y coordinate
 * @param [in,out] x    Pointer to x coordinate
 * @param dir           Direction to move
 * @return True if coordinates on input were valid and have been successfully
 *         translated, false otherwise.
 */
bool translateCell(int* y, int* x, DIRECTION dir) {
    if (*y < 0 || *y >= TERRITORY_HEIGHT ||
        *x < 0 || *x >= TERRITORY_WIDTH) {
            return false;
    }
    switch (dir) {
    case NORTH:
        if (*y == 0) { return false; }
        --*y;
        break;
    case SOUTH:
        if (*y == TERRITORY_HEIGHT-1) { return false; }
        ++*y;
        break;
    case WEST:
        if (*x == 0) { return false; }
        --*x;
        break;
    case EAST:
        if (*x == TERRITORY_WIDTH-1) { return false; }
        ++*x;
        break;
    default:
        return false;
    }
    return true;
}
