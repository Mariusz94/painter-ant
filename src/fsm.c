#include "fsm.h"

/**
 * @brief   Performs one step of FSM behaviour: finds a rule matching the
 *          current state and input signal, performs associated action and
 *          changes automaton state.
 * @param fsm    Pointer to machine description structure
 * @param input  Input signal
 * @return True if maching rule was found and executed, false otherwise
 */
bool processInput(AUTOMATON* fsm, int input) {
    int r;
    for (r = 0; r < fsm->rulesNum; r++) {
        RULE* rule = fsm->rules + r;
        if ((fsm->currentState == rule->state || rule->state == ANY_STATE) &&
            (input == rule->input || rule->input == ANY_SIGNAL)) {
            if (rule->action) {
                rule->action();
            }
            if (rule->newState != SAME_STATE) {
                fsm->currentState = rule->newState;
            }
            return true;
        }
    }
    return false;
}
